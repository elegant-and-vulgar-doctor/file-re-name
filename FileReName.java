import java.io.File;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.*;
import java.util.ArrayList;
import java.util.List;
/****
	*
	*@Author:C_Block
	*Function:文件后缀批量修改器
	*
****/
public class FileReName{
	public static void main(String[] args){
		System.out.println("运行过程中，所陈列的File:与_字符之间的空格表示子目录的缩进!");
		Scanner sc=new Scanner(System.in);
		System.out.println("Step1:请输入父目录路径名，例如d:\\java");
		File file=new File(sc.nextLine());
		System.out.println("Step2:请输入欲更改的后缀，例如jpg");
		String reg=".*\\."+sc.nextLine()+"$";
		FileReName f=new FileReName();
		f.showDirStructure(file,reg);
		System.out.println("Step3:请输入新的后缀，例如bmp");
		f.Rename(file,sc.nextLine());
		sc.close();
	}
	List<String> list=new ArrayList<String>();
	String dir="+";
    String wj="_";
    String space="";
	public void showDirStructure(File file,String reg){
		if(file.isDirectory()){
			System.out.println(space+"-------在"+dir+file.getName()+"目录下找得如下文件------");
			space+="   ";
            File[] files=file.listFiles();
            Arrays.sort(files);
            for (File f:files){
                showDirStructure(f,reg);
            }
            space="";
        }
        else{
			if(file.getName().matches(reg)){
				System.out.println("File:"+space+wj+file.getName());
				list.add(file.getName());
			}
        }
	}
	public void Rename(File file,String newEnding){
		if(file.isDirectory()){
			File[] files=file.listFiles();
			for (File f:files){
				Rename(f,newEnding);
			}
		}
		for(String name:list){
			if(file.getName().equals(name)){
				String newName = name.substring(0,name.lastIndexOf("."))+'.'+newEnding;
				File newFile = new File(file.getParent(),newName);
				System.out.println(newFile);
				file.renameTo(newFile);
			}
		}
	}
}
		