package _tools;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
/****
 *
 *@Author:C_Block
 *Function:文件后缀批量修改器
 *
 ****/
public class FileReName{
    private boolean flag=false;
    public FileReName() {
    }
    public void init(){
        System.out.println("运行过程中，所陈列的File:与_字符之间的空格表示子目录的缩进!");
        Scanner sc=new Scanner(System.in);
        System.out.println("Step1:请输入父目录路径名，例如d:\\java");
        File file=new File(sc.nextLine());
        System.out.println("Step2:请输入欲更改的后缀，例如jpg");
        String reg=".*\\."+sc.nextLine()+"$";
        if(!showDirStructure(file,reg)){
            System.out.println("null无此后缀文件！");
            return;
        }
        System.out.println("Step3:请输入新的后缀，例如bmp");
        Rename(file,sc.nextLine());
        sc.close();
    }
    List<String> list= new ArrayList<>();
    String dir="+";
    String wj="_";
    String space="";
    private boolean showDirStructure(File file,String reg){
        if(file.isDirectory()){
            File[] files=file.listFiles();
            System.out.println(space+"-------在"+dir+file.getName()+"目录下找得如下文件------");
            space+="   ";
            assert files != null;
            Arrays.sort(files);
            for (File f:files){
                showDirStructure(f,reg);
            }
            space="";
        }
        else{
            if(file.getName().matches(reg)){
                flag=true;
                System.out.println("File:"+space+wj+file.getName());
                list.add(file.getName());
            }
        }
        return flag;
    }
    private void Rename(File file,String newEnding){
        if(file.isDirectory()){
            File[] files=file.listFiles();
            assert files != null;
            for (File f:files){
                Rename(f,newEnding);
            }
        }
        for(String name:list){
            if(file.getName().equals(name)){
                String newName = name.substring(0,name.lastIndexOf("."))+'.'+newEnding;
                File newFile = new File(file.getParent(),newName);
                System.out.println(newFile);
                file.renameTo(newFile);
            }
        }
    }
}